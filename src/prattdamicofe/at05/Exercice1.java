/*
  Félix Pratt-Damico
  Atelier5 Exercice1
  DDR: 1er Novembre
 */
package prattdamicofe.at05;

public class Exercice1 {
    public static void main(String[] args) {
        int ageMere = 40, agePere = 38, ageEnfant1 = 5, ageEnfant2 = 2;
        boolean condition;

        condition = (ageMere < 50) && (ageEnfant1 < ageEnfant2); // Faux
        System.out.println("[1] condition = " + condition);

        condition = (ageMere <= 40) || (ageEnfant1 < ageEnfant2); // Vrai
        System.out.println("[2] condition = " + condition);

        condition = (agePere >= 40) || !((ageMere - agePere) > ageEnfant1); // Vrai
        System.out.println("[3] condition = " + condition);

        condition = (ageEnfant1 < ageEnfant2) && (ageEnfant1 - ageEnfant2 == 3); // Faux
        System.out.println("[4] condition = " + condition);

        condition = (ageMere > agePere) && !(ageEnfant1 > ageEnfant2); // Faux
        System.out.println("[5] condition = " + condition);
    }
}
