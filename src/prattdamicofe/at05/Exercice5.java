/*
  Félix Pratt-Damico
  Atelier5 Exercice5
  DDR: 1er Novembre
 */
package prattdamicofe.at05;

import java.util.Scanner;

public class Exercice5 {
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int revenu;
        double impotAPayer;

        System.out.print("Entrez le revenu: ");
        revenu = clavier.nextInt();

        if (revenu <= 7000) {
            impotAPayer = revenu * 0.16;
        } else if (revenu <= 14000) {
            impotAPayer = 1120 + 0.19 * (revenu - 7000);
        } else if (revenu <= 23000) {
            impotAPayer = 2450 + 0.21 * (revenu - 14000);
        } else {
            impotAPayer = 4340 + 0.23 * (revenu - 23000);
        }

        System.out.printf("Vous devez payer %.2f$ en impôt", impotAPayer);
    }
}
