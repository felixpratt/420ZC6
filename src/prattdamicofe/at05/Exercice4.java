/*
  Félix Pratt-Damico
  Atelier5 Exercice4
  DDR: 1er Novembre
 */
package prattdamicofe.at05;

import java.util.Scanner;

public class Exercice4 {
    public static void main(String[] args) {
        double note;
        char lettreNote;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez la note: ");
        note = clavier.nextDouble();

        if (note >= 90) {
            lettreNote = 'A';
        } else if (note >= 80) {
            lettreNote = 'B';
        } else if (note >= 70) {
            lettreNote = 'C';
        } else if (note>= 60) {
            lettreNote = 'D';
        } else if (note >= 50) {
            lettreNote = 'E';
        } else {
            lettreNote = 'F';
        }

        System.out.printf("%.2f%% est équivalent à un %c", note, lettreNote);
    }
}
