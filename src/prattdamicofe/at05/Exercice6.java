/*
  Félix Pratt-Damico
  Atelier5 Exercice6
  DDR: 1er Novembre
 */
package prattdamicofe.at05;

import java.util.Scanner;

public class Exercice6 {
    public static void main(String[] args) {
        boolean bisextile = false;
        int annee;
        byte mois, jours;
        String nomMois;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez le mois désiré (1-12): ");
        mois = clavier.nextByte();
        System.out.print("Entrez l'année: ");
        annee = clavier.nextInt();
        if (annee % 4 == 0 && (annee % 100 != 0 || annee % 400 == 0)) {
            bisextile = true;
        }

        jours = joursDansMois(mois, bisextile);
        nomMois = nomMois(mois);
        System.out.printf("Il y as %d jours en %s %d", jours, nomMois, annee);
    }

    /**
     * Donnes le nombre de jours du mois en prenant compte d'une année bisextile
     * @param mois Mois en chiffre (1-12)
     * @param bisextile Est-ce que l'année est bisextile?
     * @return Nombre de jours dans le mois
     */
    public static byte joursDansMois(byte mois, boolean bisextile) {
        switch (mois) {
            case 2:
                if (bisextile) {
                    return 29;
                } else {
                    return 28;
                }
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            default:
                return 30;
        }
    }

    /**
     * Donnes le nom du mois associé à la valeur donnée
     * @param mois Mois en chiffre (1-12)
     * @return Nom du mois
     */
    public static String nomMois(byte mois) {
        switch (mois) {
            case 1:
                return "Janvier";
            case 2:
                return "Février";
            case 3:
                return "Mars";
            case 4:
                return "Avril";
            case 5:
                return "Mai";
            case 6:
                return "Juin";
            case 7:
                return "Juillet";
            case 8:
                return "Août";
            case 9:
                return "Septembre";
            case 10:
                return "Octobre";
            case 11:
                return "Novembre";
            default:
                return "Décembre";
        }
    }
}
