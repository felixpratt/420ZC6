/*
  Félix Pratt-Damico
  Atelier5 Exercice7
  DDR: 1er Novembre
 */
package prattdamicofe.at05;

import java.util.Scanner;

public class Exercice7 {
    static Scanner clavier = new Scanner(System.in);

    public static void main(String[] args) {
        String figure;

        System.out.print("Choisissez la figure géométrique \nCarré, Rectangle, Cercle ou Triangle: ");
        figure = clavier.next();
        switch (figure) {
            case "Carré":
                carre();
                break;
            case "Rectangle":
                rectangle();
                break;
            case "Cercle":
                cercle();
                break;
            case "Triangle":
                triangle();
                break;
        }
    }

    /**
     * Éxécutes les étapes pour trouver le périmètre et l'aire d'un carré
     */
    public static void carre() {
        int cote, perimetre, aire;
        System.out.print("Entrez la mesure d'un côté: ");
        cote = clavier.nextInt();
        perimetre = cote * 4;
        aire = cote * cote;
        System.out.printf("Périmètre: %d \nAire: %d", perimetre, aire);
    }

    /**
     * Éxécutes les étapes pour trouver le périmètre et l'aire d'un rectangle
     */
    public static void rectangle() {
        int cote1, cote2, perimetre, aire;
        System.out.print("Entrez la mesure du premier côté: ");
        cote1 = clavier.nextInt();
        System.out.print("Entrez la mesure du deuxième côté: ");
        cote2 = clavier.nextInt();
        perimetre = 2 * cote1 + 2 * cote2;
        aire = cote1 * cote2;
        System.out.printf("Périmètre: %d \nAire: %d", perimetre, aire);
    }

    /**
     * Éxécutes les étapes pour trouver le périmètre et l'aire d'un cercle
     */
    public static void cercle() {
        int rayon;
        double perimetre, aire;
        System.out.print("Entrez le rayon du cercle: ");
        rayon = clavier.nextInt();
        perimetre = 2 * Math.PI * rayon;
        aire = Math.PI * rayon * rayon;
        System.out.printf("Périmètre: %.2f \nAire: %.2f", perimetre, aire);
    }

    public static void triangle() {
        int hauteur, largeur;
        double diagonale, perimetre, aire;
        System.out.print("Entrez la largeur de la base du triangle: ");
        largeur = clavier.nextInt();
        System.out.print("Entrez la hauteur du triangle: ");
        hauteur = clavier.nextInt();
        diagonale = Math.sqrt(Math.pow(hauteur, 2) + Math.pow((double) largeur / 2, 2));
        perimetre = largeur + diagonale * 2;
        aire = (double) (largeur * hauteur) / 2;
        System.out.printf("Périmètre: %f \nAire: %f", perimetre, aire);
    }
}
