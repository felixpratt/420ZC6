/*
  Félix Pratt-Damico
  Atelier5 Exercice2
  DDR: 1er Novembre
 */
package prattdamicofe.at05;

import java.util.Scanner;

public class Exercice2 {
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        double note;
        boolean passe;

        System.out.print("Note dans le cours de Maths: ");
        note = clavier.nextDouble();

        passe = couleOuPasse(note);

        if (passe) {
            System.out.println("Tu passes le cours de Maths!");
        } else {
            System.out.println("Tu coules le cours de Maths!");
        }
    }

    /**
     * Vérifies si un élève coule ou passe
     *
     * @param note note dans la matière
     * @return Vrai si l'élève passe, faux s'il coule
     */
    public static boolean couleOuPasse(double note) {
        return note >= 60;
    }
}
