/*
  Félix Pratt-Damico
  Atelier5 Exercice3
  DDR: 1er Novembre
 */
package prattdamicofe.at05;

import java.util.Scanner;

public class Exercice3 {
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int nb1, nb2, plusEleve;
        System.out.print("Entrez deux nombres séparés par un espace (x y): ");
        nb1 = clavier.nextInt();
        nb2 = clavier.nextInt();

        plusEleve = nombrePlusEleve(nb1, nb2);
        System.out.println("Le nombre le plus élevé est: " + plusEleve);
    }

    /**
     * Donnes le nombre le plus élevé entre les deux donnés
     *
     * @param nb1 Nombre 1
     * @param nb2 Nombre 2
     * @return Nombre le plus élevé
     */
    public static int nombrePlusEleve(int nb1, int nb2) {
        return Math.max(nb1, nb2);
    }
}
