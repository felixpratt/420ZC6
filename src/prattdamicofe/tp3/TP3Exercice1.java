/*
  Félix Pratt-Damico
  TP3Exercice1
  DDR: 1er Novembre 2021
 */
package prattdamicofe.tp3;

import java.util.Scanner;

public class TP3Exercice1 {
    public static void main(String[] args) {
        int pondTravaux, moyTravaux, pondExams, moyExams, moyenneTotale, moyenneApresDoubleSeuil;
        String sujetCoules;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Pondération des travaux: ");
        pondTravaux = clavier.nextInt();
        System.out.print("Moyenne aux travaux: ");
        moyTravaux = clavier.nextInt();

        System.out.print("Pondération des examens: ");
        pondExams = clavier.nextInt();
        System.out.print("Moyenne aux examens: ");
        moyExams = clavier.nextInt();

        verifierPonderationsDonnee(pondTravaux, pondExams);

        sujetCoules = verifierSujetsCoules(moyTravaux, moyExams);
        moyenneTotale = calculerMoyennePonderee(pondTravaux, moyTravaux, pondExams, moyExams);
        moyenneApresDoubleSeuil = calculerMoyenneApresDoubleSeuil(moyenneTotale, sujetCoules);

        affichageFinal(moyenneTotale, moyenneApresDoubleSeuil, sujetCoules);
    }

    /**
     * Calcules la moyenne pondérée
     *
     * @param pondTravaux Pondération des travaux
     * @param moyTravaux  Moyennes aux travaux
     * @param pondExams   Pondération des examens
     * @param moyExams    Moyennes aux examens
     * @return Moyenne avec les pondérations données
     */
    public static int calculerMoyennePonderee(int pondTravaux, int moyTravaux, int pondExams, int moyExams) {
        int moyenne;
        moyenne = (pondExams * moyExams + pondTravaux * moyTravaux) / 100;
        return moyenne;
    }

    /**
     * Calcules la moyenne finales en incluant le double seuil
     *
     * @param moyenne      Moyenne pondérée des examens et des travaux obtenue avec calculerMoyennePonderee()
     * @param sujetsCoules String obtenu en utilisant la méthode verifierSujetsCoules()
     * @return Moyenne finale incluant le double seuil
     */
    public static int calculerMoyenneApresDoubleSeuil(int moyenne, String sujetsCoules) {
        int moyenneApresDoubleSeuil;

        if (sujetsCoules.equals("aucuns") && moyenne > 50) {
            moyenneApresDoubleSeuil = moyenne;
        } else {
            moyenneApresDoubleSeuil = 50;
        }

        return moyenneApresDoubleSeuil;
    }

    /**
     * Vérifies les pondérations données par l'utilisateur, affiches un message d'erreur et termines le programmes si ça ne donne pas 100%
     *
     * @param pondTravaux pondération des travaux donnée par l'utilisateur
     * @param pondExams   pondération des examens donnée par l'utilisateur
     */
    public static void verifierPonderationsDonnee(int pondTravaux, int pondExams) {
        int ponderationTotale;
        ponderationTotale = pondExams + pondTravaux;
        if (ponderationTotale != 100) {
            System.out.printf("Les pondérations entrées ne sont pas valides! (Totale de %d%%)\n", ponderationTotale);
            System.exit(1);
        }
    }

    /**
     * Vérifies quels sujets sont coulés
     *
     * @param moyTravaux moyennes aux travaux donnée par l'utilisateur
     * @param moyExams   moyenne aux examens donnée par l'utilisateur
     * @return String donnant les sujets coulés
     */
    public static String verifierSujetsCoules(int moyTravaux, int moyExams) {
        String sujetsCoules = "aucuns";

        if (moyExams < 55 ^ moyTravaux < 55) {
            if (moyExams < 55) {
                sujetsCoules = "aux examens";
            } else {
                sujetsCoules = "aux travaux";
            }
        } else if (moyExams < 55) {
            sujetsCoules = "aux examens et aux travaux";
        }

        return sujetsCoules;
    }

    /**
     * Fais l'affichage final des moyennes calculées
     *
     * @param moyenneFinale           moyenne avant double seuil obtenue avec calculerMoyennePonderee()
     * @param moyenneApresDoubleSeuil moyenne après le double seuil obtenue avec calculerMoyenneApresDoubleSeuil()
     * @param sujetsCoules            String donnée par verifierSujetsCoules()
     */
    public static void affichageFinal(int moyenneFinale, int moyenneApresDoubleSeuil, String sujetsCoules) {
        String passeCoule;

        if (moyenneApresDoubleSeuil >= 60) {
            passeCoule = "SUCCÈS";
        } else {
            passeCoule = "ÉCHEC";
        }
        System.out.printf("Moyenne finale calculée de l'étudiant: %d%%\n", moyenneFinale);
        if (sujetsCoules.equals("aucuns")) {
            System.out.printf("Moyenne finale de l’étudiant avec le double seuil: %d%%  %s", moyenneApresDoubleSeuil, passeCoule);
        } else {
            System.out.printf("Moyenne %s inférieure à 55%% \nMoyenne finale de l’étudiant avec le double seuil: %d%%  %s",
                    sujetsCoules, moyenneApresDoubleSeuil, passeCoule);
        }
    }
}
