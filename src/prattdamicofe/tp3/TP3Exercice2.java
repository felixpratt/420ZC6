/*
  Félix Pratt-Damico
  TP3Exercice2
  DDR: 1er Novembre 2021
 */
package prattdamicofe.tp3;

import java.util.Scanner;

public class TP3Exercice2 {
    static Scanner clavier = new Scanner(System.in);

    public static void main(String[] args) {
        byte ageClient;
        String jourDeLaSemaine;

        System.out.print("Entrez votre age: ");
        ageClient = clavier.nextByte();

        if (ageClient <= 5) {
            // Catégorie 0-5 ans
            demandePasse(0, 0);
        } else if (ageClient <= 11) {
            // Catégorie 6-11 ans
            System.out.print("Entrez la journée de la semaine: ");
            jourDeLaSemaine = clavier.next();

            if (jourDeLaSemaine.equals("samedi") || jourDeLaSemaine.equals("dimanche")) {
                // Jour de fin de semaine
                demandePasse(0, 52);
            } else {
                // Jour de semaine
                demandePasse(2.5, 52);
            }
        } else if (ageClient <= 17) {
            // 12-17 ans
            demandePasse(2.5, 52);
        } else {
            // 18 ans et +
            System.out.print("Êtes-vous étudiant (oui/non): ");
            if (clavier.next().equals("oui")) {
                // Étudiant
                demandePasse(3.5, 52);
            } else if (ageClient <= 64) {
                // 18-64 ans
                demandePasse(3.5, 86.5);
            } else {
                // 65 ans et plus
                demandePasse(2.5, 52);
            }
        }
    }

    /**
     * Demandes si c'est pour un passage ou une passe mensuelle, affiches le prix
     *
     * @param prixPassage prix pour un passage
     * @param prixPasse   prix pour la passe mensuelle
     */
    public static void demandePasse(double prixPassage, double prixPasse) {
        String passeOuPassage;
        // Si le prix de la passe mensuelle est à 0$ alors elle est indisponible
        if (prixPasse == 0) {
            System.out.print("Passage gratuit");
        } else {
            System.out.print("Passe mensuelle ou passage: ");
            passeOuPassage = clavier.next();

            // Si c'est un passage et qu'il est gratuit
            if (passeOuPassage.equals("passage") && prixPassage == 0) {
                System.out.print("Passage gratuit");
            // Si le passage n'est pas gratuit mais que c'est tout de même un passage
            } else if (passeOuPassage.equals("passage")) {
                System.out.printf("Le coût d'un passage est de %.2f$", prixPassage);
            } else {
                System.out.printf("Le coût pour la passe mensuelle est de %.2f$", prixPasse);
            }
        }
    }
}