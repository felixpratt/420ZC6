package prattdamicofe.at03.ex02;

import java.util.Scanner;

public class Facture2Produits {
	public static final double TPS = 0.05, TVQ = 0.09975;

	public static void main(String[] args) {
		Scanner clavier = new Scanner(System.in);

		double total, totalProduit, prixProduit;
		int quantiteProduit;

		System.out.println("Entrez le prix du premier produit :");
		prixProduit = clavier.nextDouble();
		System.out.println("Entrez la quantite achetee :");
		quantiteProduit = clavier.nextInt();
		totalProduit = prixProduit * quantiteProduit;
		total = totalProduit;
		total = total + totalProduit * TPS;
		total = total + totalProduit * TVQ;
		System.out.println("Entrez le prix du second produit :");
		prixProduit = clavier.nextDouble();
		System.out.println("Entrez la quantite achetee :");
		quantiteProduit = clavier.nextInt();
		totalProduit = prixProduit * quantiteProduit;
		total = total + totalProduit;
		total = total + totalProduit * TPS;
		total = total + totalProduit * TVQ;
		System.out.println(String.format("Le total de la facture est %,.2f$.", total));
	}
}