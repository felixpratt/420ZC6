package prattdamicofe.at07;

import java.util.Scanner;

public class Exercice2 {
    public static void main(String[] args) {
        long factorielle;
        int n;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez le nombre à faire la factorielle: ");
        n = clavier.nextInt();

        factorielle = n;
        for (int i = n - 1; i != 0; i--) {
            factorielle *= i;
        }

        System.out.printf("La factorielle de %d est %d", n, factorielle);
    }
}
