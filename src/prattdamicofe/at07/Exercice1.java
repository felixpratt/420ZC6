package prattdamicofe.at07;

import java.util.Scanner;

public class Exercice1 {
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int x, n;
        long resultat;

        System.out.print("Entrez la base: ");
        x = clavier.nextInt();
        System.out.print("Entrez l'exposant: ");
        n = clavier.nextInt();

        resultat = x;
        for (int i = 1; i < n; i++) {
            resultat *= x;
        }
        System.out.printf("Le résultat est de %d", resultat);
    }
}
