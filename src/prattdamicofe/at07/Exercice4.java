package prattdamicofe.at07;

import java.util.Scanner;

public class Exercice4 {
    public static void main(String[] args) {
        int n;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez un nombre entier: ");
        n = clavier.nextInt();

        // Carré
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }

        System.out.println();

        // Triangle
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }

    public static void dessinerCarre(int nbLignes) {

    }
}
