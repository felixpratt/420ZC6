package prattdamicofe.at07;

import java.util.Scanner;

public class Exercice6 {
    public static void main(String[] args) {
        int n, nombreDeFibonacci;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez un entier: ");
        n = clavier.nextInt();

        nombreDeFibonacci = calculerNombreDeFibonacci(n);
        System.out.printf("Le nombre de fibonacci est de: %d", nombreDeFibonacci);
    }

    public static int calculerNombreDeFibonacci(int n) {
        if (n == 0 || n == 1) {
            return n;
        }
        return calculerNombreDeFibonacci(n - 1) + calculerNombreDeFibonacci(n - 2);
    }
}
