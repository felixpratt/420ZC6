package prattdamicofe.at07;

import java.util.Scanner;

public class Exercice5 {
    public static void main(String[] args) {
        int n;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez un entier positif: ");
        n = clavier.nextInt();

        System.out.print(n);
        while (n != 1) {
            if (n % 2 == 0) {
                n /= 2;
            } else {
                n = (n * 3) + 1;
            }
            System.out.printf(", %d", n);
        }
    }
}
