package prattdamicofe.at07;

public class Exercice3 {
    public static void main(String[] args) {
        for (int i = 0; i <= 100; i += 2) {
            System.out.printf("\n%d² = %d", i, i * i);
        }
    }
}
