package prattdamicofe.at04;

import java.util.Scanner;

public class Cercle {
    public static void main(String[] args) {
        double rayon, aire, perimetre;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez le rayon: ");
        rayon = clavier.nextFloat();

        perimetre = 2 * Math.PI * rayon;
        aire = Math.PI * rayon * rayon;

        System.out.println("Le périmètre est de " + perimetre);
        System.out.println("L'aire est de " + aire);
    }
}
