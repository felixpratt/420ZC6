package prattdamicofe.at04;

import java.util.Scanner;

public class Permutation {
    public static void main(String[] args) {
        String a, b, temp;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Inscrire un premier mot: ");
        a = clavier.next();
        System.out.print("Inscrire un deuxième mot: ");
        b = clavier.next();

        System.out.println("mot1 = " + a + " mot2 = " + b);
        temp = a;
        a = b;
        b = temp;
        System.out.println("mot1 = " + a + " mot2 = " + b);
    }
}
