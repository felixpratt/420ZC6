package prattdamicofe.at04;

import java.util.Scanner;

public class Salaire {
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int nbHeures;
        float tauxHoraire, salaire;
        System.out.print("Nombre d'heures : ");
        nbHeures = clavier.nextInt();
        System.out.print("Taux horaire : ");
        tauxHoraire = clavier.nextFloat();
        salaire = nbHeures * tauxHoraire;
        System.out.println("Le salaire est de : " + salaire);
    }
}
