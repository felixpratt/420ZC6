package prattdamicofe.at04;

import java.util.Scanner;

public class Balle {
    public static void main(String[] args) {
        int posX, posY, speedX, speedY;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez la position initiale en X de la balle: ");
        posX = clavier.nextInt();
        System.out.print("Entrez la position initiale en Y de la balle: ");
        posY = clavier.nextInt();
        System.out.print("Entrez la vitesse en X de la balle: ");
        speedX = clavier.nextInt();
        System.out.print("Entrez la vitesse en Y de la balle: ");
        speedY = clavier.nextInt();

        System.out.printf("Position initiale: (%d, %d)", posX, posY);
        for (int i = 1; i < 4; i++) {
            posX = posX + speedX;
            posY = posY + speedY;
            System.out.printf("\nPosition après %d seconde(s): (%d, %d)", i, posX, posY);
        }
    }
}
