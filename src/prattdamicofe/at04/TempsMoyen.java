package prattdamicofe.at04;

import java.util.Scanner;

public class TempsMoyen {
    public static int NB_TRAJETS = 3;

    public static void main(String[] args) {
        int temps, tempsTotal;
        double moyenne;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez la durée du premier trajet: ");
        temps = clavier.nextInt();
        tempsTotal = temps;

        System.out.print("Entrez la durée du second trajet: ");
        temps = clavier.nextInt();
        tempsTotal = tempsTotal + temps;

        System.out.print("Entrez la durée du troisième trajet: ");
        temps = clavier.nextInt();
        tempsTotal = tempsTotal + temps;

        moyenne = (float) tempsTotal / NB_TRAJETS;
        System.out.print("Le temps moyen de parcours est " + moyenne);
    }
}
