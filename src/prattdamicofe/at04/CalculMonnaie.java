package prattdamicofe.at04;

import java.util.Scanner;

public class CalculMonnaie {
    public static void main(String[] args) {
        int nb100S, nb20S, nb5S, nb1S, cout, montantPaye, montantARemette;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Coût total de l'achat: ");
        cout = clavier.nextInt();

        System.out.print("Montant payé par le client: ");
        montantPaye = clavier.nextInt();

        montantARemette = montantPaye - cout;
        nb100S = montantARemette / 100;
        montantARemette = montantARemette % 100;
        nb20S = montantARemette / 20;
        montantARemette = montantARemette % 20;
        nb5S = montantARemette / 5;
        nb1S = montantARemette % 5;

        System.out.print("Remettre au client " + nb100S + "x100$, " + nb20S + "x20$, " + nb5S + "x5$ et " + nb1S + "x1$.");
    }
}
