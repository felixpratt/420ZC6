/*
    Félix Pratt-Damico
    TP5 Exercice1
    DDR: 17 Décembre 2021
 */
package prattdamicofe.tp5;

import java.util.Scanner;

public class Exercice1 {
    static Scanner clavier = new Scanner(System.in);

    public static void main(String[] args) {
        String[] nomsEtudiants = new String[10];
        int[] notes = new int[10];
        int nbEtudiants = 0;
        byte choixMenu;

        do {
            System.out.println("*****************************************");
            System.out.println("Que voulez-vous faire?");
            System.out.println("1- Saisir une note");
            System.out.println("2- Afficher les étudiants et les notes");
            System.out.println("3- Calculer la moyenne");
            System.out.println("4- Trouver la note d'un étudiant");
            System.out.println("5- Quitter");
            System.out.println("*****************************************");
            System.out.print("Entrez votre choix: ");
            choixMenu = clavier.nextByte();

            switch (choixMenu) {
                case 1:
                    if (nbEtudiants < 10) {
                        if (saisirNote(nomsEtudiants, notes, nbEtudiants)) {
                            nbEtudiants += 1;
                        }
                    } else {
                        System.out.println("Vous avez déjà entré 10 notes!");
                    }
                    break;
                case 2:
                    afficher(nomsEtudiants, notes, nbEtudiants);
                    break;
                case 3:
                    moyenne(notes, nbEtudiants);
                    break;
                case 4:
                    trouverNote(nomsEtudiants, notes, nbEtudiants);
            }
        } while (choixMenu != 5);
    }

    /**
     * Demandes à l'utilisateur les informations et ajoutes un nouveau étudiant et sa note
     *
     * @param noms        Tableau de noms d'étudiants
     * @param notes       Tableau de notes d'étudiants
     * @param nbEtudiants compteur du nombre d'étudiants dans le système
     * @return True si nom/note ajoutée, false si le nom se trouvait déjà dans la list
     */
    public static boolean saisirNote(String[] noms, int[] notes, int nbEtudiants) {
        String nouveauNom;
        System.out.print("Entrez le nom de l'étudiant: ");
        nouveauNom = clavier.next();
        if (nbEtudiants > 0) {
            for (int i = 0; i < nbEtudiants; i++) {
                if (noms[i].equalsIgnoreCase(nouveauNom)) {
                    System.out.println("Ce nom est déjà dans le système!");
                    return false;
                }
            }
        }
        noms[nbEtudiants] = nouveauNom;
        System.out.printf("Entrez la note de %s: ", noms[nbEtudiants]);
        notes[nbEtudiants] = clavier.nextInt();
        return true;
    }

    /**
     * Affichers tout les noms d'étudiants et leurs notes, affiches une erreur si nbEtudiants=0
     *
     * @param noms        Tableau de noms d'étudiants
     * @param notes       Tableau de notes d'étudiants
     * @param nbEtudiants compteur du nombre d'étudiants dans le système
     */
    public static void afficher(String[] noms, int[] notes, int nbEtudiants) {
        if (nbEtudiants != 0) {
            for (int i = 0; i < nbEtudiants; i++) {
                System.out.printf("%-20s %s%%\n", noms[i], notes[i]);
            }
        } else {
            System.out.println("Le tableau est vide!");
        }
    }

    /**
     * Affiches la moyenne des notes des étudiants, affiches une erreur si nbEtudiants=0
     *
     * @param notes       Tableau de notes d'étudiants
     * @param nbEtudiants compteur du nombre d'étudiants dans le système
     */
    public static void moyenne(int[] notes, int nbEtudiants) {
        double moyenne;
        int total = 0;
        if (nbEtudiants != 0) {
            for (int i = 0; i < nbEtudiants; i++) {
                total += notes[i];
            }
            moyenne = (double) total / nbEtudiants;
            System.out.printf("La moyenne est de %.2f%%\n", moyenne);
        } else {
            System.out.println("Le tableau est vide!");
        }
    }

    /**
     * Demandes à l'utilisateur d'entrer un nom d'étudiant et la méthode affiche la note de l'étudiant
     *
     * @param noms        Tableau de noms d'étudiants
     * @param notes       Tableau de notes d'étudiants
     * @param nbEtudiants compteur du nombre d'étudiants dans le système
     */
    public static void trouverNote(String[] noms, int[] notes, int nbEtudiants) {
        String nomCherche;
        if (nbEtudiants != 0) {
            System.out.print("Entrez le nom: ");
            nomCherche = clavier.next();
            for (int i = 0; i <= nbEtudiants; i++) {
                if (nomCherche.equalsIgnoreCase(noms[i])) {
                    System.out.printf("La note de %s est %d%%\n", noms[i], notes[i]);
                    break;
                } else if (i == nbEtudiants) {
                    System.out.println("L'étudiant n'as pas été trouvé");
                }
            }
        } else {
            System.out.println("Le tableau est vide!");
        }
    }
}
