package prattdamicofe.at04v2;

import java.util.Scanner;

public class Salaire {
    public static void main(String[] args) {
        float nbHeures, tauxHoraire, salaire;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Nombre d'heures: ");
        nbHeures = clavier.nextFloat();
        System.out.print("Taux horaire: ");
        tauxHoraire = clavier.nextFloat();

        salaire = calculSalaire(nbHeures, tauxHoraire);
        System.out.printf("Le salaire est de: %f", salaire);
    }

    /**
     * Calcules le salaire
     * @param nbHeures nombre d'heures
     * @param tauxHoraire taux horaire
     * @return salaire calculé
     */
    public static float calculSalaire(float nbHeures, float tauxHoraire) {
        return nbHeures * tauxHoraire;
    }
}
