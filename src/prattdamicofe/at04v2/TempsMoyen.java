package prattdamicofe.at04v2;

import java.util.Scanner;

public class TempsMoyen {
    public static int NB_TRAJETS = 3;

    public static void main(String[] args) {
        int temps, tempsTotal;
        float moyenne;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez la durée du premier trajet: ");
        temps = clavier.nextInt();
        tempsTotal = temps;

        System.out.print("Entrez la durée du second trajet: ");
        temps = clavier.nextInt();
        tempsTotal = tempsTotal + temps;

        System.out.print("Entrez la durée du troisième trajet: ");
        temps = clavier.nextInt();
        tempsTotal = tempsTotal + temps;

        moyenne = moyenneTrajets(tempsTotal);
        System.out.print("Le temps moyen de parcours est " + moyenne);
    }

    /**
     * Calcules la moyenne des trajets avec la constante NB_TRAJETS
     * @param tempsTotal temps de tout les trajets
     * @return moyenne calculée
     */
    public static float moyenneTrajets(int tempsTotal) {
        return (float) tempsTotal / NB_TRAJETS;
    }
}
