package prattdamicofe.at04v2;

import java.util.Scanner;

public class TableMultiplication {
    public static void main(String[] args) {
        int table;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Inscrivez la table voulue: ");
        table = clavier.nextInt();
        tableMult(table);
    }

    /**
     * Affiches la table de multiplication de 1 à 12 pour l'input donné
     * @param input La table de multiplication à afficher
     */
    public static void tableMult(int input) {
        int result;
        System.out.println("*************************");
        for (int i = 1; i <= 12; i++) {
            result = input * i;
            System.out.printf("%d X %d = %d\n", input, i, result);
        }
        System.out.println("*************************");
    }
}
