package prattdamicofe.at04v2;

import java.util.Scanner;

public class FactureRabais {
    static Scanner clavier = new Scanner(System.in);

    public static void main(String[] args) {
        double prixProduit, rabais;
        prixProduit = demanderPrix();
        rabais = demanderRabais();
        total(rabais, prixProduit);
    }

    /**
     * Demandes le prix du produit
     * @return double étant le prix du produit
     */
    public static double demanderPrix() {
        System.out.print("Entrez le prix du produit: ");
        return clavier.nextDouble();
    }

    /**
     * Demandes le rabais en pourcent
     * @return le pourcentage en nombre à virgule
     */
    public static double demanderRabais() {
        double rabais;
        System.out.print("Entrez le pourcentage du rabais: ");
        rabais = clavier.nextInt();
        rabais = rabais*0.01;
        return rabais;
    }

    /**
     * Calcules le rabais et affiches les valeurs
     * @param rabais rabais en nombre à virgule (ex 0.05 pour 5%)
     * @param prixProduit prix du produit
     */
    public static void total(double rabais, double prixProduit) {
        double montantRabais, prixApresRabais;
        montantRabais = rabais * prixProduit;
        prixApresRabais = prixProduit - montantRabais;
        System.out.printf("Prix Original: %.2f$ \nMontant du rabais: %.2f$ \nPrix après le rabais: %.2f$", prixProduit, montantRabais, prixApresRabais);
    }
}
