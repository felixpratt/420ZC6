package prattdamicofe.at04v2;

public class LigneEtoile {
    public static void main(String[] args) {
        ligneEtoiles();
        ligneEtoiles();
        ligneEtoiles();
    }

    /**
     * Affiches une ligne de 25 étoiles
     */
    public static void ligneEtoiles() {
        System.out.println("*************************");
    }
}
