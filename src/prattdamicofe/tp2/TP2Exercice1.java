/*
  Félix Pratt-Damico
  TP2Exercice1
  DDR: 18 Octobre 2021
 */
package prattdamicofe.tp2;

import java.util.Scanner;

public class TP2Exercice1 {
    static final float CADEUR = 0.65f;

    public static void main(String[] args) {
        double montant, cad, eur;
        montant = demanderMontant();
        eur = canadienVersEuro(montant);
        cad = euroVersCanadien(eur);
    }

    /**
     * Demandes à l'utilisateur d'entrer un montant et lis le montant
     * @return Le montant lu
     */
    public static double demanderMontant() {
        double montant;
        Scanner clavier = new Scanner(System.in);
        System.out.print("Entrez un montant: ");
        montant = clavier.nextDouble();

        return montant;
    }

    /**
     * Convertis des dollars canadiens en Euros
     * @param cad Le montant en dollars canadiens
     * @return Le montant en euros
     */
    public static double canadienVersEuro(double cad) {
        double eur;
        eur = cad * CADEUR;
        System.out.printf("\n%.2f$ équivaut à %.2f euros", cad, eur);

        return eur;
    }

    /**
     * Convertis des euros en dollars canadiens
     * @param eur Le montant en euros
     * @return Le montant en dollars canadiens
     */
    public static double euroVersCanadien(double eur) {
        double cad;
        cad = eur/CADEUR;
        System.out.printf("\n%.2f euros équivaut à %.2f$", eur, cad);

        return cad;
    }
}
