/*
  Félix Pratt-Damico
  TP2Exercice2
  DDR: 18 Octobre 2021
 */
package prattdamicofe.tp2;

import java.util.Scanner;

public class TP2Exercice2 {
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        double xA, yA, xB, yB, xC, yC, segmentAB, segmentBC, segmentAC, perimetre, aire;

        System.out.print("Entrez les positions en x et y du premier point (A), séparées par un espace (x y): ");
        xA = clavier.nextDouble();
        yA = clavier.nextDouble();
        System.out.print("Entrez les positions en x et y du deuxième point (B), séparées par un espace (x y): ");
        xB = clavier.nextDouble();
        yB = clavier.nextDouble();
        System.out.print("Entrez les positions en x et y du troisième point (C), séparées par un espace (x y): ");
        xC = clavier.nextDouble();
        yC = clavier.nextDouble();

        ligne25Etoiles();

        segmentAB = longueurSegment(xA, yA, xB, yB);
        System.out.printf("Mesure du segment AB: %.3f", segmentAB);
        segmentBC = longueurSegment(xB, yB, xC, yC);
        System.out.printf("\nMesure du segment BC: %.3f", segmentBC);
        segmentAC = longueurSegment(xA, yA, xC, yC);
        System.out.printf("\nMesure du segment AC: %.3f", segmentAC);

        perimetre = perimetreTriangle(segmentAB, segmentBC, segmentAC);
        System.out.printf("\nPérimètre du triangle ABC: %.3f", perimetre);

        aire = aireTriangle(segmentAB, segmentBC, segmentAC);
        System.out.printf("\nAire du triangle ABC: %.3f", aire);
    }

    /**
     * Affiches une ligne de 25 étoiles (*) à la console
     */
    public static void ligne25Etoiles() {
        System.out.println("*************************");
    }

    /**
     * Calcules la longueur d'un segment d'un triangle
     * @param x1 La position X du premier point
     * @param y1 La position Y du premier point
     * @param x2 La position X du deuxième point
     * @param y2 La position Y du deuxième point
     * @return Longeur du segment entre les deux points
     */
    public static double longueurSegment(double x1, double y1, double x2, double y2) {
        double longeur;
        longeur = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));

        return longeur;
    }

    /**
     * Calcules le périmètres du triangle
     * @param segment1 Longueur du premier segment
     * @param segment2 Longueur du deuxième segment
     * @param segment3 Longueur du troisième segment
     * @return Périmètre du triangle
     */
    public static double perimetreTriangle(double segment1, double segment2, double segment3) {
        double perimetre;
        perimetre = segment1 + segment2 + segment3;

        return perimetre;
    }

    /**
     * Calcules l'aire du triangle
     * @param segment1 Longueur du premier segment
     * @param segment2 Longueur du deuxième segment
     * @param segment3 Longueur du troisième segment
     * @return Aire du triangle
     */
    public static double aireTriangle(double segment1, double segment2, double segment3) {
        double demiPerimetre, aire;

        demiPerimetre = (segment1 + segment2 + segment3) / 2;
        aire = Math.sqrt(demiPerimetre * (demiPerimetre - segment1) * (demiPerimetre - segment2) * (demiPerimetre - segment3));

        return aire;
    }
}
