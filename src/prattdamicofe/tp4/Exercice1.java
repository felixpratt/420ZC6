/*
    Félix Pratt-Damico
    TP4 Exercice1
    DDR: 26 Novembre 2021
 */
package prattdamicofe.tp4;

import java.util.Scanner;

public class Exercice1 {
    public static void main(String[] args) {
        byte nombreADeviner, nombreDevine, tentative = 1;
        Scanner clavier = new Scanner(System.in);

        System.out.println("Essayez le deviner le nombre entre 1 et 10, vous avez 4 tentatives!");
        nombreADeviner = genererNombreAleatoire();
        do {
            System.out.printf("Tentative #%d: ", tentative);
            nombreDevine = clavier.nextByte();
            if (nombreDevine > nombreADeviner) {
                System.out.println("Trop grand");
            } else if (nombreDevine < nombreADeviner) {
                System.out.println("Trop petit");
            }
            tentative++;
        } while (tentative <= 4 && nombreADeviner != nombreDevine);
        affichageFinal(nombreADeviner, nombreDevine);
    }

    /**
     * Génères un nombre aléatoire entre 1 et 10
     *
     * @return nombre aléatoire entre 1 et 10
     */
    public static byte genererNombreAleatoire() {
        return (byte) (Math.random() * 10 + 1);
    }

    /**
     * Affiches le nombre à deviner et dis à l'utilisateur si il a réussi ou non
     *
     * @param nombreADeviner nombre qui devait être deviné
     * @param nombreDevine   nombre deviné par l'utilisateur
     */
    public static void affichageFinal(byte nombreADeviner, byte nombreDevine) {
        if (nombreDevine == nombreADeviner) {
            System.out.println("Bravo vous avez deviné!");
        } else {
            System.out.println("Perdu, vous avez utilisé 4 tentatives");
        }
        System.out.printf("Le nombre à deviner était %d", nombreADeviner);
    }
}
