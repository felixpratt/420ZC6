/*
    Félix Pratt-Damico
    TP4 Exercice2
    DDR: 26 Novembre 2021
 */
package prattdamicofe.tp4;

import java.util.Scanner;

public class Exercice2 {
    static Scanner clavier = new Scanner(System.in);

    public static void main(String[] args) {
        byte optionChoisie;

        // J'ai choisi la boucle do...while car le menu doit s'afficher en premier et continuer de s'afficher tant que l'utilisateur n'as pas choisi de quitter
        do {
            System.out.print("MENU PRINCIPAL \n1- Placement, 2- Prévision, 3- Quitter: ");
            optionChoisie = clavier.nextByte();

            switch (optionChoisie) {
                case 1:
                    placement();
                    break;
                case 2:
                    prevision();
                    break;
                case 3:
                    System.out.println("Merci d'avoir utilisé le programme!");
                    break;
                default:
                    System.out.println("Option Invalide!");
            }
        } while (optionChoisie != 3);
    }

    /**
     * Méthode pour calculer un placement sur un nombre d'années spécifiées
     */
    public static void placement() {
        int capitalDepart, capitalActuel, anneesPlacement, interet;
        double tauxInteret;

        System.out.print("Capital de départ: ");
        capitalDepart = clavier.nextInt();
        System.out.print("Taux d'intérêt annuel (ex 5 pour 5%): ");
        tauxInteret = (clavier.nextDouble() * 0.01);

        System.out.print("Durée du placement (en années): ");
        anneesPlacement = clavier.nextInt();
        // J'ai choisi la boucle while car elle s'exécute seulement si la condition est vraie
        while (anneesPlacement <= 0) {
            System.out.println("Le nombre d'années doit être de 1 minimum");
            System.out.print("Durée du placement (en années): ");
            anneesPlacement = clavier.nextInt();
        }

        capitalActuel = capitalDepart;
        affichageColonnes("Année", "Capital", "Intérêt", "Nouveau capital");
        System.out.println("------------------------------------------------");
        // J'ai choisi la boucle for puisque l'on sait le nombre d'itérations dès le départ
        for (int i = 1; i <= anneesPlacement; i++) {
            interet = (int) (capitalActuel * tauxInteret);
            affichageColonnes(String.valueOf(i), capitalActuel + "$", interet + "$", capitalActuel + interet + "$");
            capitalActuel += interet;
        }
    }

    /**
     * Méthode pour calculer combien d'années un placement prendra pour atteindre le montant spécifié
     */
    public static void prevision() {
        int capitalDepart, capitalActuel, montantDesire, interet, i = 0;
        double tauxInteret;

        System.out.print("Capital de départ: ");
        capitalDepart = clavier.nextInt();
        System.out.print("Taux d'intérêt annuel (ex 5 pour 5%): ");
        tauxInteret = (clavier.nextDouble() * 0.01);

        System.out.print("Montant désiré: ");
        montantDesire = clavier.nextInt();
        // J'ai choisi la boucle while car elle s'exécute seulement si la condition est vraie
        while (montantDesire <= capitalDepart) {
            System.out.println("Le montant désiré doit être suppérieur au capital de départ");
            System.out.print("Montant désiré: ");
            montantDesire = clavier.nextInt();
        }

        capitalActuel = capitalDepart;
        affichageColonnes("Année", "Capital", "Intérêt", "Nouveau capital");
        System.out.println("------------------------------------------------");
        /*
        J'ai choisi la boucle while car je n'ai pas besoin de faire un affichage avant de comparer
        et je ne peut pas utiliser ma condition booléenne dans une boucle for
         */
        while (capitalActuel < montantDesire) {
            interet = (int) (capitalActuel * tauxInteret);
            affichageColonnes(String.valueOf(i + 1), capitalActuel + "$", interet + "$", capitalActuel + interet + "$");
            capitalActuel += interet;

            i++;
        }
        System.out.printf("Vous devez placer votre argent pour %d ans afin d'atteindre le montant voulu\n", i);
    }

    /**
     * Fait l'affichage à la console en 4 colonnes alignées
     *
     * @param colonne1 String à afficher dans la colonne 1
     * @param colonne2 String à afficher dans la colonne 2
     * @param colonne3 String à afficher dans la colonne 3
     * @param colonne4 String à afficher dans la colonne 4
     */
    public static void affichageColonnes(String colonne1, String colonne2, String colonne3, String colonne4) {
        System.out.printf("%-10s %-10s %-10s %-10s\n", colonne1, colonne2, colonne3, colonne4);
    }
}
