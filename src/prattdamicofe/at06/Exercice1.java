/*
    Félix Pratt-Damico
    Atelier 6 Exercice 1
    DDR: 8 Novembre 2021
 */
package prattdamicofe.at06;

import java.util.Scanner;

public class Exercice1 {

    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        byte heuresTravailles;
        double salaire, tauxHoraire;

        System.out.print("Entrez le nombre d'heures travaillées: ");
        heuresTravailles = clavier.nextByte();

        while (!(30 <= heuresTravailles && 50 >= heuresTravailles)) {
            System.out.println("Le nombre d'heures doit être entre 30 et 50");
            System.out.print("Entrez le nombre d'heures travaillées: ");
            heuresTravailles = clavier.nextByte();
        }

        System.out.print("Entrez votre taux horaire: ");
        tauxHoraire = clavier.nextDouble();

        salaire = calculerSalaire(heuresTravailles, tauxHoraire);

        System.out.printf("Le salaire est de %.2f$", salaire);
    }

    /**
     * Calcules le salaire avec le nombre d'heures et le taux horaire donné
     *
     * @param heures      nombre d'heures travaillées
     * @param tauxHoraire salaire horaire de la personne
     * @return salaire calculé
     */
    public static double calculerSalaire(byte heures, double tauxHoraire) {
        double salaire;

        salaire = heures * tauxHoraire;

        return salaire;
    }
}
