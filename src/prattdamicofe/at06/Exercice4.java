/*
    Félix Pratt-Damico
    Atelier 6 Exercice 4
    DDR 8 Novembre 2021
 */
package prattdamicofe.at06;

import java.util.Scanner;

public class Exercice4 {
    public static void main(String[] args) {
        int base, exposant = 1, valeur;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez la base: ");
        base = clavier.nextInt();
        System.out.print("Entrez la valeur cherchée: ");
        valeur = clavier.nextInt();

        while (Math.pow(base, exposant + 1) <= valeur) {
            exposant++;
        }

        System.out.printf("L'exposant recherché est %d donnant %.0f, car %d donne %.0f", exposant, Math.pow(base, exposant), exposant + 1, Math.pow(base, exposant + 1));
    }
}
