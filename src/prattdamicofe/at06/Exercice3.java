/*
    Félix Pratt-Damico
    Atelier 6 Exercice 3
    DDR: 8 Novembre 2021
 */
package prattdamicofe.at06;

import java.util.Scanner;

public class Exercice3 {
    public static void main(String[] args) {
        int somme = 0, nombreEntre;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez un nombre positif: ");
        nombreEntre = clavier.nextInt();
        do {
            if (nombreEntre < 0) {
                System.out.println("Le nombre doit être positif!");
            } else {
                somme += nombreEntre;
            }
            System.out.print("Entrez un nombre positif (ou -1 pour terminer): ");
            nombreEntre = clavier.nextInt();
        } while (nombreEntre != -1);
        System.out.printf("Somme des nombre entrés: %d", somme);
    }
}
