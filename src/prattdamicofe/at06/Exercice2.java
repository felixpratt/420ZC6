/*
    Félix Pratt-Damico
    Atelier 6 Exercice 2
    DDR: 8 Novembre 2021
 */
package prattdamicofe.at06;

import java.util.Scanner;

public class Exercice2 {
    public static void main(String[] args) {
        int somme = 0, valeurDonnee;
        char calculerSomme;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Voulez-vous calculer une somme? (Oui/Non): ");
        calculerSomme = clavier.next().charAt(0);

        if (calculerSomme == 'O' || calculerSomme == 'o') {
            System.out.print("Entrez un nombre positif: ");
            valeurDonnee = clavier.nextInt();

            while (valeurDonnee != -1) {
                if (valeurDonnee < 0) {
                    System.out.println("Le nombre doit être positif!");
                } else {
                    somme += valeurDonnee;
                }
                System.out.print("Entrez un nombre positif (ou -1 pour terminer): ");
                valeurDonnee = clavier.nextInt();
            }
            System.out.printf("La somme est de %d", somme);
        } else {
            System.out.print("Dommage...");
        }
    }
}
