/*
    Félix Pratt-Damico
    Atelier 6 Exercice 5
    DDR: 8 Novembre 2021
 */
package prattdamicofe.at06;

import java.util.Scanner;

public class Exercice5 {
    public static void main(String[] args) {
        int nb1, nb2;
        double resultat = 0;
        String entree, operateur;
        Scanner clavier = new Scanner(System.in);

        System.out.print("Entrez une équation (ex 5 * 5) ou \"Quitter\" pour quitter: ");
        entree = clavier.next();

        while (entree.charAt(0) != 'Q' && entree.charAt(0) != 'q') {
            nb1 = Integer.parseInt(entree);
            operateur = clavier.next();
            nb2 = clavier.nextInt();
            switch (operateur) {
                case "+":
                    resultat = nb1 + nb2;
                    break;
                case "-":
                    resultat = nb1 - nb2;
                    break;
                case "*":
                    resultat = nb1 * nb2;
                    break;
                case "/":
                    resultat = (double) nb1 / nb2;
                    break;
                case "%":
                    resultat = nb1 % nb2;
                    break;
                case "^":
                    resultat = Math.pow(nb1, nb2);
                    break;
            }
            System.out.printf("%d%s%d=%.2f\n", nb1, operateur, nb2, resultat);

            System.out.print("Entrez une équation (ex 5 * 5) ou \"Quitter\" pour quitter: ");
            entree = clavier.next();
        }
    }
}
