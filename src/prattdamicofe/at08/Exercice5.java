/*
    Félix Pratt-Damico
    Atelier 8 Exercice 5
    DDR: 29 Novembre 2021
 */
package prattdamicofe.at08;

import java.util.Scanner;

public class Exercice5 {
    public static void main(String[] args) {
        String[] mots;
        Scanner clavier = new Scanner(System.in);
        int nbMots, compteurBonjour = 0;

        System.out.print("Combien de mots voulez vous entrer? ");
        nbMots = clavier.nextInt();
        mots = new String[nbMots];

        for (int i = 0; i < nbMots; i++) {
            System.out.printf("Entrez le mot #%d: ", i + 1);
            mots[i] = clavier.next();
        }

        for (String mot : mots) {
            if (mot.equals("bonjour")) {
                compteurBonjour += 1;
            }
        }

        System.out.printf("Le mot \"bonjour\" se retrouves %d fois dans le tableau", compteurBonjour);
    }
}
