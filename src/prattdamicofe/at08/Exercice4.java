/*
    Félix Pratt-Damico
    Atelier 8 Exercice 4
    DDR: 29 Novembre 2021
 */
package prattdamicofe.at08;

import java.util.Scanner;

public class Exercice4 {
    public static void main(String[] args) {
        String[] mots;
        Scanner clavier = new Scanner(System.in);
        int nbMots;
        int indiceBonjour = -1;

        System.out.print("Combien de mots voulez vous entrer? ");
        nbMots = clavier.nextInt();
        mots = new String[nbMots];

        for (int i = 0; i < nbMots; i++) {
            System.out.printf("Entrez le mot #%d: ", i + 1);
            mots[i] = clavier.next();
        }

        for (int i = 0; i < mots.length; i++) {
            if (mots[i].equals("bonjour")) {
                indiceBonjour = i;
            }
        }

        System.out.println("Le mot \"bonjour\" est à l'indice " + indiceBonjour);
    }
}
