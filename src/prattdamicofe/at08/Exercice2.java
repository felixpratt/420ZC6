/*
    Félix Pratt-Damico
    Atelier 8 Exercice 2
    DDR: 29 Novembre 2021
 */
package prattdamicofe.at08;

import java.util.Scanner;

public class Exercice2 {
    public static void main(String[] args) {
        String[] mots;
        Scanner clavier = new Scanner(System.in);
        int nbMots;

        System.out.print("Combien de mots voulez vous entrer? ");
        nbMots = clavier.nextInt();
        mots = new String[nbMots];

        for (int i = 0; i < nbMots; i++) {
            System.out.printf("Entrez le mot #%d: ", i + 1);
            mots[i] = clavier.next();
        }

        for (int i = nbMots - 1; i >= 0; i--) {
            System.out.printf("\nLe mot #%d: %s", i + 1, mots[i]);
        }
    }
}
