/*
    Félix Pratt-Damico
    Atelier 8 Exercice 6
    DDR: 29 Novembre 2021
 */
package prattdamicofe.at08;

import java.util.Scanner;

public class Exercice6 {
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        String[] noms = new String[5];
        int[] notes = new int[5];

        for (int i = 0; i < noms.length; i++) {
            System.out.printf("Entrez le nom de l'élève #%d: ", i + 1);
            noms[i] = clavier.next();
            System.out.printf("Entrez la note de %s: ", noms[i]);
            notes[i] = clavier.nextInt();
        }

        for (int i = 0; i < noms.length; i++) {
            System.out.printf("\n%s: %d%%", noms[i], notes[i]);
        }
    }
}
