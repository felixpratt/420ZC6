package prattdamicofe.exam1;

import java.util.Scanner;

public class LabrancheDansLarbre {
    static final int SALAIREBASE = 500;
    static final float TAUXIMPOT = 0.25f;

    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int nbBranches;
        double salaire, impotPaye, salaireApresImpot;
        String nom;

        System.out.print("Nom: ");
        nom = clavier.next();
        System.out.print("Nombre de branches coupée(s): ");
        nbBranches = clavier.nextInt();
        salaire = SALAIREBASE + 20 * nbBranches;
        impotPaye = salaire * TAUXIMPOT;
        salaireApresImpot = salaire - impotPaye;
        System.out.printf("Nom: %s \nSalaire avant impôt: %.2f$ \nImpôt payé: %.2f$ \nSalaire net: %.2f$", nom, salaire, impotPaye, salaireApresImpot);
    }
}
