package prattdamicofe.exam1;

public class CalculNotes {
    public static void main(String[] args) {
        int note1 = 20, note2 = 30, note3 = 40, note4 = 50;
        double moyenneEntiers;

        moyenneEntiers = moyenneSomme(note1, note2, note3, note4);
        System.out.printf("\nMoyenne des 4 notes: %f", moyenneEntiers);
    }

    /**
     * Calcules la moyenne de quatre entiers et affiche la somme
     * @param entier1 Premier Entier
     * @param entier2 Deuxième Entier
     * @param entier3 Troisième Entier
     * @param entier4 Quarième Entier
     * @return La moyenne des 4 entiers
     */
    public static double moyenneSomme(int entier1, int entier2, int entier3, int entier4) {
        double moyenne;
        int somme;

        somme = entier1 + entier2 + entier3 + entier4;
        moyenne = (double) somme / 4;

        System.out.printf("Somme des 4 entiers: %d", somme);
        return moyenne;
    }
}
